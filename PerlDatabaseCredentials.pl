#!/bin/perl
# change this if you want to place this file somewhere else
# credpath is path of this pl script.
$credpath =~ s/(.*)PerlDatabaseCredentials\.pl/$1/;
my $file = "$credpath.credentials";

## try it differently. 
if (!-e $file) {
	$credpath = abs_path("../.LoadCredentials.pl");
	$credpath =~ s/(.*)PerlDatabaseCredentials\.pl/$1/;
	$file = "$credpath.credentials";
}
	
# read in config file
our %config;
open CONFIG, $file or die "can't open $file: $!";
while (<CONFIG>) {
    chomp;
    s/#.*//; # Remove comments
    s/^\s+//; # Remove opening whitespace
    s/\s+$//;  # Remove closing whitespace
    next unless length;
    my ($key, $value) = split(/\s*=\s*/, $_, 2);
    $config{$key} = $value;
}
close CONFIG;
# SET PATH
our $path = `echo \$PATH`;
chomp($path);
if ($config{'PATH'} ne '') {
	$path = $config{'PATH'}.":$path";
}
## username and password 
our $userid = $config{'DBUSER'};
our $dbuser = $userid;
our $userpass = $config{'DBPASS'};
our $dbpass = $userpass;
our $host = $config{'DBHOST'};
our $dbhost = $host;
our $scriptuser = $config{'SCRIPTUSER'};
our $scriptpass= $config{'SCRIPTPASS'};
## paths
our $sitedir = $config{'SITEDIR'};
$sitedir =~ s/\/$//;
our $scriptdir = $config{'SCRIPTDIR'};
$scriptdir =~ s/\/$//;
our $scriptdirreg = $scriptdir;
$scriptdirreg =~ s/\//\\\//g;
our $datadir = $config{'DATADIR'};
$datadir =~ s/\/$//;
our $datadirreg =~ s/\//\\\//g;
our $basepath = $config{'WEBPATH'};
our $ftpdir = '';
if (defined($config{'FTP_DATA'})) {
	$ftpdir = $config{'FTP_DATA'};
}
# Decipher Credentials
# admin email 
our $adminemail = $config{'ADMINMAIL'};
#cluster settings
our $usehpc = 0;
if (defined($config{'USEHPC'})) {
	$usehpc = $config{'USEHPC'};
}
our $queue = 'batch';
our $account = '';
if ($usehpc == 1) {
	if (defined($config{'QUEUE'}) && $config{'QUEUE'} ne '') {
		$queue = $config{'QUEUE'};
	}
	if (defined($config{'ACCOUNT'}) && $config{'ACCOUNT'} ne '') {
		$account = $config{'ACCOUNT'};
	}
}

our $mysqlthreads = 1;
if (defined($config{'MYSQLTHREADS'})) {
	$mysqlthreads = $config{'MYSQLTHREADS'};
}
## memcached settings
our $usemc = 0;
if (defined($config{'USEMEMCACHED'})) {
	$usemc = $config{'USEMEMCACHED'};
	if (defined($config{'MCS'})) {
		@mcs = split(/,/,$config{'MCS'});
		if (scalar(@mcs) == 0) {
			$usemc = 0;
		}
	}
	else {
		$usemc = 0;
	}

}

## you need to manually connect to DB, as it requires host (set here) as well as db-name (not set here) ! 

# exit with success code

1;
