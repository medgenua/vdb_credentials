<?php
$sessionid = session_id();
if (empty($sessionid)) {
    session_start();
}
// change this line if you want to place this file somewhere else !
$file = realpath(dirname(__FILE__)) . "/.credentials";
$lines = file($file) or die("Could not read $file"); //return(0);
$config = array();
foreach ($lines as $line_num => $line) {
    # Comment?
    if (!preg_match("/#.*/", $line) && !preg_match("/^\[/", $line)) {
        # Contains non-whitespace?
        if (preg_match("/\S/", $line)) {
            list($key, $value) = explode("=", trim($line), 2);
            $config[$key] = $value;
        }
    }
}

## users and passwords
$user = $config['DBUSER'];
$pw = $config['DBPASS'];
$host = $config['DBHOST'];
if (array_key_exists('DBSUFFIX', $config)) {
    $db_suffix = "_" . $config['DBSUFFIX'];
} else {
    $db_suffix  = '';
}
$scriptuser = $config['SCRIPTUSER'];
$scriptpass = $config['SCRIPTPASS'];
// base path 
$basepath = $config['WEBPATH'];
## admin email
$adminemail = $config['ADMINMAIL'];
## paths

$scriptdir = $config['SCRIPTDIR'];
$scriptdir = preg_replace('%(.*)/$%', "$1", $scriptdir);
$scriptdirreg = preg_replace('%/%', '\/', $scriptdir);
$sitedir = $config['SITEDIR'];
$sitedir = preg_replace('%(.*)/$%', "$1", $sitedir);
$datadir = $config['DATADIR'];
#$maintenancedir = $config['MAINTENANCEDIR'];
// HPC settings
if (isset($config['USEHPC'])) {
    $usehpc = $config['USEHPC'];
} else {
    $usehpc = 0;
}
// memcached is disabled.
$usemc = 0;
//$queuename = $config['QUEUENAME'];
// connect to database
if (!isset($_GET['page']) || $_GET['page'] == '' || !isset($_SESSION['dbname'])) {
    $mysqli = new mysqli($host, $user, $pw, "NGS-Variants-Admin$db_suffix"); //or die("could not connect to $host");
    if ($mysqli->connect_errno) {
        echo "Failed to connect to MySQL: " . $mysqli->connect_error;
        exit();
    }
    $result = $mysqli->query("SELECT name, StringName, Description FROM `CurrentBuild` LIMIT 1");
    // no results
    if ($result->num_rows === 0) {
        die("Could not retrieve Database version");
    }
    // get results
    $row = $result->fetch_assoc();
    $_SESSION['dbsuffix'] = $db_suffix;
    $_SESSION['dbname'] = $row['name'] . $db_suffix;
    $_SESSION['dbstring'] = $row['StringName'];
    $_SESSION['dbdescription'] = stripslashes($row['Description']);
    $result->free();
}


if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 'main';
}

// ADD CONDA TO PATH
$path = '';
if (isset($config['CONDA_ENV'])) {
    $path .= ":" . $config['CONDA_ENV'] . "/bin";
}
if (isset($config['PATH'])) {
    $path .= ":" . $config['PATH'];
}
// add micromamba 
$path .= ":" . $config["SCRIPTDIR"] . "/dependencies/micromamba";


// export the TIP of code
if (isset($_SESSION['tip_ui'])) {
    $tip = $_SESSION['tip_ui'];
} elseif (isset($_SESSION['fresh_session']) && $_SESSION['fresh_session'] == 1) {
    $command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $sitedir && git rev-parse --short HEAD \" 2>/dev/null)";
    exec($command, $output, $exit);
    $tip = $output[0];
    $_SESSION['tip_ui'] = $tip;
    $_SESSION['fresh_session'] = 0;
} else {
    $tip = "-";
}
// if in debug mode, add random value to tip
if (isset($config['LOG_LEVEL']) && $config['LOG_LEVEL'] == 'DEBUG') {
    $tip .= rand();
}
// set alive
$_SESSION['last_activity'] = time();
